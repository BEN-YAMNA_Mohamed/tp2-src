package com.beny.tp2;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class WineActivity extends AppCompatActivity {
    Wine wine;
    EditText name;
    EditText region;
    EditText localization;
    EditText climate;
    EditText area;
    Button save;
    WineDbHelper WineDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);
        WineDbHelper = new WineDbHelper(getApplicationContext());
        Intent intent = getIntent();
        final Bundle extras = getIntent().getExtras();


        wine = (Wine) extras.get("wine");

        name = (EditText) findViewById(R.id.wineName) ;
        region = (EditText) findViewById(R.id.editWineRegion) ;
        localization = (EditText) findViewById(R.id.editLoc) ;
        climate = (EditText) findViewById(R.id.editClimate) ;
        area = (EditText) findViewById(R.id.editPlantedArea) ;

        if(wine != null) {
            name.setText(wine.getTitle());
            region.setText(wine.getRegion());
            localization.setText(wine.getLocalization());
            climate.setText(wine.getClimate());
            area.setText(wine.getPlantedArea());
        }

        save = (Button) findViewById(R.id.button);

        Button save = findViewById(R.id.button);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Wine ajout = new Wine(name.getText().toString(), region.getText().toString(), localization.getText().toString(), climate.getText().toString(), area.getText().toString());

                if(extras.get("value").equals("add")){
                    MainActivity.w.addWine(ajout);
                }
                else{
                    wine.setTitle(name.getText().toString());
                    wine.setClimate(climate.getText().toString());
                    wine.setLocalization(localization.getText().toString());
                    wine.setPlantedArea(area.getText().toString());
                    wine.setRegion(region.getText().toString());
                    MainActivity.w.updateWine(wine);

                }
                Toast.makeText(getApplicationContext(), "Enregistre", Toast.LENGTH_SHORT).show();
                Intent in2 = new Intent(WineActivity.this, MainActivity.class);
                startActivity(in2);


            }




    });
}
}