package com.beny.tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class WineDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WineDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "wine.db";

    public static final String TABLE_NAME = "cellar";

    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localization";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "publisher";

    public WineDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_WINE_TABLE = "CREATE TABLE " +
                TABLE_NAME + "("
                + _ID + " TEXT PRIMARY KEY," + COLUMN_NAME
                + " TEXT," + COLUMN_WINE_REGION + " TEXT," + COLUMN_LOC
                + " TEXT,"+ COLUMN_CLIMATE
                + " TEXT," + COLUMN_PLANTED_AREA
                + " TEXT" + ')';
        db.execSQL(CREATE_WINE_TABLE);
        // db.execSQL() with the CREATE TABLE ... command
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    /**
     * Adds a new wine
     * @return  true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    public boolean addWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, wine.getTitle());
        values.put(COLUMN_WINE_REGION, wine.getRegion());
        values.put(COLUMN_LOC, wine.getLocalization());
        values.put(COLUMN_CLIMATE, wine.getClimate());
        values.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());
        // Inserting Row
        long rowID = 0;
        // call db.insert()
        db.insertOrThrow(TABLE_NAME, null, values);

        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */
    public int updateWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();
        int res;

        // updating row
        ContentValues contv = new ContentValues();
        contv.put(_ID,wine.getId());
        contv.put(COLUMN_NAME,wine.getTitle());
        contv.put(COLUMN_WINE_REGION,wine.getRegion());
        contv.put(COLUMN_LOC,wine.getLocalization());
        contv.put(COLUMN_CLIMATE,wine.getClimate());
        contv.put(COLUMN_PLANTED_AREA,wine.getPlantedArea());

        res = db.update(TABLE_NAME, contv, _ID + " = "+ wine.getId(), null);
        // call db.update()

        return res;
    }

    /**
     * Returns a cursor on all the wines of the library
     */
    public Cursor fetchAllWines() {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor;

        String sql = String.format(
                "SELECT * FROM %s",
                TABLE_NAME
        );
        cursor = db.rawQuery(sql, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        db.close();
        return cursor;
    }

    public void deleteWine(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        // call db.delete();
        db.delete(TABLE_NAME, _ID + " = ?", new String[] {String.valueOf(cursor.getLong(0))});
        db.close();
    }

    public void populate() {
        Log.d(TAG, "call populate()");
        addWine(new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"));
        addWine(new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"));
        addWine(new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"));
        addWine(new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"));
        addWine(new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"));
        addWine(new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"));
        addWine(new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"));
        addWine(new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"));
        addWine(new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"));
        addWine(new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"));
        addWine(new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"));



        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }


    public static Wine cursorToWine(Cursor cursor) {
        int id_index = cursor.getColumnIndexOrThrow(_ID);
        int title_index = cursor.getColumnIndexOrThrow(COLUMN_NAME);
        int region_index = cursor.getColumnIndexOrThrow(COLUMN_WINE_REGION);
        int localization_index = cursor.getColumnIndexOrThrow(COLUMN_LOC);
        int climate_index = cursor.getColumnIndexOrThrow(COLUMN_CLIMATE);
        int plantedarea_index = cursor.getColumnIndexOrThrow(COLUMN_PLANTED_AREA);

        long id = (Long) cursor.getLong(id_index);

        String title = (String) cursor.getString(title_index);
        String region= (String) cursor.getString(region_index);
        String localization = (String) cursor.getString(localization_index);
        String climate = (String) cursor.getString(climate_index);
        String plantedarea = (String) cursor.getString(plantedarea_index);

        return new Wine (id, title, region, localization, climate, plantedarea);
    }
}
