package com.beny.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {
    ListView listview;
    SimpleCursorAdapter adapter;
    public static WineDbHelper w;
    Cursor c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        FloatingActionButton fab = findViewById(R.id.fab);


        w = new WineDbHelper(this);
        //w.populate();
        listview = (ListView) findViewById(R.id.listv);
        String[] from = {w.COLUMN_NAME,w.COLUMN_WINE_REGION,
                w.COLUMN_LOC,w.COLUMN_CLIMATE,w.COLUMN_PLANTED_AREA};
        int[] to = { android.R.id.text1 , android.R.id.text2 };
        c = w.fetchAllWines();

         adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_1, c ,
                from,
                to);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent in1 = new Intent(MainActivity.this,WineActivity.class);
                Cursor c2 = (Cursor) listview.getItemAtPosition(position);
                Wine temp = WineDbHelper.cursorToWine(c2);
                in1.putExtra("wine", temp);
                in1.putExtra("value","update");
                startActivity(in1);
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in2 = new Intent(v.getContext(), WineActivity.class);
                in2.putExtra("wine", new Wine());
                in2.putExtra("value","add");
                startActivity(in2);

            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        c.moveToPosition(info.position);
        w.deleteWine(c);
        c = w.fetchAllWines();
        adapter.changeCursor(c);
        adapter.notifyDataSetChanged();
        return true;
    }
}
